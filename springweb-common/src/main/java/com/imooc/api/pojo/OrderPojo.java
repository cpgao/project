package com.imooc.api.pojo;

import lombok.Data;

import java.util.List;

@Data
public class OrderPojo {
    private Integer orderSum;
    private List<Object> orderlist;
}
