package com.imooc.api.order;

import com.imooc.api.pojo.OrderPojo;

import java.util.List;


/**
 * 定义订单相关接口
 */
public interface OrderApiService {
    /**
     * 定义获得订单数量接口
     * @return
     */
    public Integer selectOrderCount();

    /**
     * 下单接口
     */
    public void saveOrder();
}
