package com.imooc.mmall.service.impl;

import com.imooc.api.order.OrderApiService;
import com.imooc.mmall.dao.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("orderApiService")
public class OrderApiServiceImpl implements OrderApiService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Integer selectOrderCount() {
        return orderMapper.selectList().size();
    }

    @Override
    public void saveOrder() {
        log.info("我是下单接口");
    }
}
