package com.imooc.mmall.service.impl;

import com.imooc.mmall.dao.OrderMapper;
import com.imooc.mmall.pojo.Order;
import com.imooc.mmall.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("iOrderService")
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> selectList() {
        return orderMapper.selectList();
    }

    @Override
    public List<Order> selectByOrderNoAndStatus(String orderNo, List<Integer> statusList) {
        return orderMapper.selectByOrderNoAndStatus(orderNo, statusList);
    }
}
