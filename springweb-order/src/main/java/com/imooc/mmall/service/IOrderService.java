package com.imooc.mmall.service;

import com.imooc.mmall.pojo.Order;

import java.util.List;

public interface IOrderService {
    List<Order> selectList();

    List<Order> selectByOrderNoAndStatus(String orderNo, List<Integer> statusList);
}
