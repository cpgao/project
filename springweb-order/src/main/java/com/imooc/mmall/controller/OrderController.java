package com.imooc.mmall.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imooc.common.ServerResponse;
import com.imooc.mmall.pojo.Order;
import com.imooc.mmall.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by geely
 */
@Slf4j
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService iOrderService;


//    http://localhost:8081/order/getOrderPageList/1/3
    /**
     * 查询订单信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/getOrderPageList/{pageNum}/{pageSize}",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<PageInfo> getOrderPageList(
            @PathVariable("pageNum") int pageNum,
            @PathVariable("pageSize") int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<Order> productList = iOrderService.selectList();
        PageInfo pageResult = new PageInfo(productList);
        return ServerResponse.createBySuccess("查询成功",pageResult);
    }

//    http://localhost:8081/order/getOrderPageListByMap?orderNo=&status=&pageNum=1&pageSize=5&orderBy=payment_desc

    /**
     * 根据条件，查询订单信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/getOrderPageListByMap",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<PageInfo> getOrderPageListByMap(
            @RequestParam(value = "orderNo",required = false)String orderNo,
            @RequestParam(value = "status",required = false)Integer status,
            @RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
            @RequestParam(value = "pageSize",defaultValue = "10") int pageSize,
            @RequestParam(value = "orderBy",defaultValue = "") String orderBy
    ){
        log.info("订单分页查询接口——start——参数orderNo={},status={},pageNum={},pageSize={},orderBy={},"
                ,orderNo,status,pageNum,pageSize,orderBy);

        PageHelper.startPage(pageNum,pageSize);
        List<Integer> statusList = null;
        if(status!=null){
            statusList = new ArrayList<Integer>();
            statusList.add(status);
        }
        //排序处理
        if(StringUtils.isNotBlank(orderBy)){
            if("payment_desc".equals(orderBy)){
                String[] orderByArray = orderBy.split("_");
                PageHelper.orderBy(orderByArray[0]+" "+orderByArray[1]);
            }
        }
        List<Order> productList = iOrderService.selectByOrderNoAndStatus(orderNo,statusList);
        PageInfo pageResult = new PageInfo(productList);
        return ServerResponse.createBySuccess("查询成功",pageResult);


    }


}
