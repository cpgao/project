package com.imooc.mmall.service;


import com.imooc.common.ServerResponse;
import com.imooc.mmall.pojo.User;

/**
 * Created by geely
 */
public interface IUserService {

    int getUserCount();

    User selectByPrimaryKey(Integer id);

    ServerResponse<User> login(String username, String password);

    ServerResponse<User> saveUser(User user);
}
