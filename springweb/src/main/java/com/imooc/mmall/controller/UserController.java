package com.imooc.mmall.controller;

import com.imooc.common.ServerResponse;
import com.imooc.mmall.common.Const;
import com.imooc.mmall.pojo.User;
import com.imooc.mmall.service.IUserService;
import com.imooc.mmall.transactiona.UserServiceJTA;
import com.imooc.redis.RedisPoolUtil;
import com.imooc.redis.RedisShardedPoolUtil;
import com.imooc.utils.CookieUtil;
import com.imooc.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Created by geely
 */
@Slf4j
@Controller
@RequestMapping("/user")
public class UserController {


    @Autowired
    private IUserService iUserService;
    @Autowired
    private UserServiceJTA userServiceJTA;


    /**
     * 测试读写分离
     * @return
     */
    @RequestMapping(value = "/getUserCount",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<Integer> getUserCount(){
        return ServerResponse.createBySuccess("查询成功",iUserService.getUserCount());
    }

    /**
     * mycat分片测试
     * @return
     */
    @RequestMapping(value = "/saveUser",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> saveUser(@RequestBody User user){
        return iUserService.saveUser(user);
    }

    /**
     * 测试分布式事物-多数据愿
     * @return
     */
    @RequestMapping(value = "/saveUserJTA",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> saveUserJTA(@RequestBody User user) throws SQLException {
        return userServiceJTA.saveUser(user);
    }



    /**
     * 查询用户
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}/info",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<User> login(@PathVariable("id") Integer id){
        return ServerResponse.createBySuccess("查询成功",iUserService.selectByPrimaryKey(id));
    }


    /**
     * 用户登录
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(
            @RequestParam(value = "username",required = false)String username,
            @RequestParam(value = "password",required = false)String password,
                                      HttpSession session, HttpServletResponse httpServletResponse){
        //验证登陆
        ServerResponse<User> response = iUserService.login(username,password);
        if(response.isSuccess()){
            //将数据写入cookie和redis中
            CookieUtil.writeLoginToken(httpServletResponse,session.getId());
            //此处也可使用单台—RedisPoolUtil存储 RedisShardedPoolUtil
            RedisPoolUtil.setEx(session.getId(),
                    JsonUtil.obj2String(response.getData()),
                    Const.RedisCacheExtime.REDIS_SESSION_EXTIME);
        }
        return response;
    }

    @RequestMapping(value = "/logout",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
        CookieUtil.delLoginToken(httpServletRequest,httpServletResponse);
        RedisPoolUtil.del(loginToken);
        return ServerResponse.createBySuccess();
    }


}
