package com.imooc.mmall.controller;


import com.imooc.api.order.OrderApiService;
import com.imooc.common.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 测试double-order-服务
 */
@Slf4j
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderApiService orderApiService;


//    http://localhost:8080/order/getOrderCount

    /**
     * 测试double-order-订单数量
     * @return
     */
    @RequestMapping(value = "/getOrderCount",method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<Integer> getOrderCount(){
        Integer count=orderApiService.selectOrderCount();
        return ServerResponse.createBySuccess("订单数量查询成功",count);
    }


}
