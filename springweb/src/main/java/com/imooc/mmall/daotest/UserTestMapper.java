package com.imooc.mmall.daotest;

import com.imooc.mmall.pojo.User;


public interface UserTestMapper {

    int insert(User record);
}