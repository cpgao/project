package com.imooc.mmall.dao;

import com.imooc.mmall.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    List<Order> selectList();

    List<Order>selectByOrderNoAndStatus(@Param("orderNo")String orderNo, @Param("statusList")List<Integer> statusList);

}