package com.imooc.mmall.exception;

public class UserException extends RuntimeException {

    public UserException(String s) {
        super(s);
    }
}
