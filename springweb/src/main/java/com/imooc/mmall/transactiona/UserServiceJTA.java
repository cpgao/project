package com.imooc.mmall.transactiona;

import com.imooc.common.ServerResponse;
import com.imooc.mmall.dao.UserMapper;
import com.imooc.mmall.daotest.UserTestMapper;
import com.imooc.mmall.pojo.User;
import com.imooc.mq.activemq.ActivemqSendMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;


/**
 * Created by geely
 */
@Service("userServiceJTA")
public class UserServiceJTA {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserTestMapper userTestMapper;

    @Autowired
    private ActivemqSendMessage activeMqSendMessage;


//    @Transactional(value = "jtaTransactionManager",rollbackFor = Exception.class)   已开启aop不需要次注解
    @Transactional
    public ServerResponse<User> saveUser(User user) throws SQLException {
        user.setCreateTime(new Date());
        user.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        userMapper.insert(user);

        User a = new User();
        a.setPassword("111");
        a.setUsername("tom");
        a.setRole(0);
        a.setCreateTime(new Date());
        a.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        userTestMapper.insert(a);

        activeMqSendMessage.sendActiveQueue(" 你好我是用户创建消息");

        if("error".equals(user.getPassword())){
            //模拟抛出异常
            LocalTranJdbcApplication.createError();
        }

        return ServerResponse.createBySuccess("保存成功",user);
    }

}
