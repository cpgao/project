package com.imooc.mmall.transactiona;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
/**
 * jdbc事物处理
 */
public class LocalTranJdbcApplication2 {

    private static final Logger LOG = LoggerFactory.getLogger(LocalTranJdbcApplication2.class);

    public static void main(String[] args) throws SQLException {

        String sql = "SELECT * FROM mmall_user FOR UPDATE";
        String plusAmountSQL = "UPDATE mmall_user SET role = ? WHERE username = ?";

        Connection dbConnection = LocalTranJdbcApplication.getDBConnection();
        LOG.debug("Begin session2");

        PreparedStatement queryPS = dbConnection.prepareStatement(sql);
        ResultSet rs = queryPS.executeQuery();
        int superManAmount = 0;
        while (rs.next()) {
            String name = rs.getString("username");
            int role = rs.getInt("role");
            if (name.equals("admin")) {
                superManAmount = role;
            }
        }

        PreparedStatement updatePS = dbConnection.prepareStatement(plusAmountSQL);
        updatePS.setLong(1, superManAmount + 100);
        updatePS.setString(2, "admin");
        updatePS.executeUpdate();

        LOG.debug("Done session2!");
        queryPS.close();
        updatePS.close();
        dbConnection.close();
    }


}
