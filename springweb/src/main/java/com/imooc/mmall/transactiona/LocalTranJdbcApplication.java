package com.imooc.mmall.transactiona;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * jdbc事物处理
 */
public class LocalTranJdbcApplication {

    public static void main(String[] args) throws SQLException {


        String plusAmountSQL = "UPDATE mmall_user SET role = role + 100 WHERE username = ?";
        String minusAmountSQL = "UPDATE mmall_user SET role = role - 100 WHERE username = ?";

        Connection dbConnection = getDBConnection();
        System.out.println("Begin");
        dbConnection.setAutoCommit(false);

        PreparedStatement plusAmountPS = dbConnection.prepareStatement(plusAmountSQL);
        plusAmountPS.setString(1, "admin");
        plusAmountPS.executeUpdate();

        createError();

        PreparedStatement minusAmountPS = dbConnection.prepareStatement(minusAmountSQL);
        minusAmountPS.setString(1, "tom");
        minusAmountPS.executeUpdate();

        dbConnection.commit();
        System.out.println("ok");

        plusAmountPS.close();
        minusAmountPS.close();
        dbConnection.close();
    }

    public static void createError() throws SQLException {
        throw new SQLException("Simulate some error!");
    }

    public static Connection getDBConnection() throws SQLException {
        String DB_DRIVER = "com.mysql.jdbc.Driver";
        String DB_CONNECTION = "jdbc:mysql://localhost:3306/mmall";
        String DB_USER = "root";
        String DB_PASSWORD = "Abc123++";
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
    }
}
