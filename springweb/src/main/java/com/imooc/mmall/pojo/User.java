package com.imooc.mmall.pojo;

import lombok.*;

import java.util.Date;

@Data
@EqualsAndHashCode
@AllArgsConstructor //全参构造
@NoArgsConstructor //无参构造
public class User {
    private Integer id;

    private String username;

    private String password;

    private String email;

    private String phone;

    private String question;

    private String answer;

    private Integer role;

    private Date createTime;

    private Date updateTime;

}