package com.imooc.controller;

import com.imooc.common.ServerResponse;
import lombok.extern.slf4j.Slf4j;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Slf4j
@Controller
//@RequestMapping("/hello/")
public class HelloController {


    @ResponseBody
    @RequestMapping(value = "/hi")
    public ServerResponse<String> hello(){
        return ServerResponse.createBySuccess("hello");
    }

    //logback日志输出
    @ResponseBody
    @RequestMapping(value = "/log")
    public ServerResponse<String> lombak(){
        log.debug("调试信息debug");
        log.info("一般信息info");
        log.warn("警告信息warn");
        log.error("错误信息error");
//        try {
            double d=1/0;
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//        }
        return ServerResponse.createBySuccess("hello");
    }

    //log4j2日志输出
//    private static Logger logger = LogManager.getLogger(HelloController.class.getName());
//    @ResponseBody
//    @RequestMapping(value = "/log4j")
//    public ServerResponse<String> log4j(){
//        logger.debug("调试信息debug");
//        logger.info("一般信息info");
//        logger.warn("警告信息warn");
//        logger.error("错误信息error");
//        logger.fatal("致命错误信息error");
//        try {
//            double d=1/0;
//        }catch (Exception e){
//            logger.error(e.getMessage(),e);
//        }
//        return ServerResponse.createBySuccess("hello");
//    }


}
