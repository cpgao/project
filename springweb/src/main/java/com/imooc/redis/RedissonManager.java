package com.imooc.redis;

import com.imooc.utils.PropertiesUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Redisson 初始化
 */
@Slf4j
@Component
public class RedissonManager {

    private static String redisIp1 = PropertiesUtil.getProperty("redis1.ip");
    private static Integer redisPort1 = Integer.parseInt(PropertiesUtil.getProperty("redis1.port"));

    private static String redisIp2 = PropertiesUtil.getProperty("redis2.ip");
    private static Integer redisPort2 = Integer.parseInt(PropertiesUtil.getProperty("redis2.port"));

    private Config config = new Config();
    private Redisson redisson = null;

    //注入到Spring容器的话，使用@PostConstruct或者静态块初始化，效果是一样的{}
    //@PostConstruct
    public void init() {
        try {
            //在redis环境没有搭建起来之前，这里先注释上，否则项目启动不起来。
            //单台模式
            config.useSingleServer().setAddress(redisIp1+":"+redisPort1);
//            //单主模式
//            config.useMasterSlaveServers().setMasterAddress(redisIp1+":"+redisPort1);
//            //主从模式
//            config.useMasterSlaveServers()
//                    .setMasterAddress(redisIp1+":"+redisPort1)
//                    .addSlaveAddress(redisIp2+":"+redisPort2);
            redisson = (Redisson) Redisson.create(config);
            log.info("初始化Redisson结束");
        } catch (Exception e) {
            log.error("redisson init error", e);
        }
    }

    public Redisson getRedisson() {
        return redisson;
    }


    public static void main(String[] args) {
//        单机版
//        Config c = new Config();
//        c.useSingleServer()
//        .setAddress(new StringBuilder().append("192.168.207.130").append(":").append("6380").toString())
//        .setPassword("123456");
//        Redisson r = (Redisson) Redisson.create(c);
//
//        RSet<String> set = r.getSet("anySet");
//        set.add("set1111");
//        set.add("set2222");
//
//        RAtomicLong atomicLong = r.getAtomicLong("myAtomicLong"); atomicLong.set(3);
//        atomicLong.incrementAndGet();
//        System.out.println("myAtomicLong="+atomicLong.get());
//        r.shutdown();


//        private SentinelServersConfig sentinelServersConfig;//基本哨兵server的配置
//        private MasterSlaveServersConfig masterSlaveServersConfig;//基于主从的配置
//        private SingleServerConfig singleServerConfig;//基于单台连接的配置
//        private ClusterServersConfig clusterServersConfig;//基于集群的配置

//        Config c = new Config();
//        c.useClusterServers().getNodeAddresses() .addNodeAddress("192.168.207.130:6379","192.168.207.130:6380")
//        .setPassword("123456");
//        Redisson r = (Redisson) Redisson.create(c);
//        for (int i = 0; i <10 ; i++) {
//            RAtomicLong atomicLong = r.getAtomicLong("myAtomicLong"+i); atomicLong.set(100);
//            atomicLong.incrementAndGet();
//        }
//        r.shutdown();
    }





}
