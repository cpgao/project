package com.imooc.mq.rabbitmq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MQConfig {

	public static final String QUEUE = "queue";
	public static final String QUEUE_MSG = "queue.msg";
	public static final String QUEUE_JMS = "queue.jms";
	public static final String QUEUE_NEW = "queue.new";

	public static final String TOPIC_EXCHANGE = "topicExchage";
	public static final String TOPIC_QUEUE1 = "topic.queue1";
	public static final String TOPIC_QUEUE2 = "topic.queue2";

	public static final String HEADER_QUEUE = "header.queue";
	public static final String FANOUT_EXCHANGE = "fanoutxchage";
	public static final String HEADERS_EXCHANGE = "headersExchage";

	/**
	 * Direct模式 交换机Exchange   直连模式
	 * */
	@Bean
	public Queue queue() {
		return new Queue(QUEUE, true);
	}
	@Bean
	public Queue queueMessage() {
//		Map<String, Object> arguments = new HashMap<String, Object>();
//		在队列中延迟30s后，将该消息重新投递到x-dead-letter-exchange对应的Exchange中
//		arguments.put("x-dead-letter-exchange", QUEUE_MSG);
//		arguments.put("x-message-ttl", 30 * 1000);
//		Queue的5个参数说明
//		@param name队列的名称-不能为空；设置为“”以让代理生成名称。
//		@param.bletrue如果我们声明一个持久队列（该队列将在服务器重新启动后存活）
//		@param.ve true如果我们声明一个独占队列（队列将只由声明者的连接使用）
//		@param自动删除true，如果服务器应该在不再使用时删除队列
//		“PARAM”参数用于声明队列的参数
//		return new Queue(QUEUE_MSG, true,false,false,arguments);
		return new Queue(QUEUE_MSG, true);
	}
	@Bean
	public Queue queueJms() {
		return new Queue(QUEUE_JMS, true);
	}
	@Bean
	public Queue queueNew() {
		return new Queue(QUEUE_NEW, true);
	}


	/**
	 * Topic模式 交换机Exchange
	 * */
	@Bean
	public Queue topicQueue1() {
		return new Queue(TOPIC_QUEUE1, true);
	}
	@Bean
	public Queue topicQueue2() {
		return new Queue(TOPIC_QUEUE2, true);
	}
	@Bean
	public TopicExchange topicExchage(){
		return new TopicExchange(TOPIC_EXCHANGE);
	}
	@Bean
	public Binding topicBinding1() {
		return BindingBuilder.bind(topicQueue1()).to(topicExchage()).with("topic.key1");
	}
	@Bean
	public Binding topicBinding2() {
		return BindingBuilder.bind(topicQueue2()).to(topicExchage()).with("topic.#");
	}



	/**
	 * Fanout模式 交换机Exchange   广播模式
	 * */
	@Bean
	public FanoutExchange fanoutExchage(){
		return new FanoutExchange(FANOUT_EXCHANGE);
	}
	@Bean
	public Binding FanoutBinding1() {
		return BindingBuilder.bind(topicQueue1()).to(fanoutExchage());
	}
	@Bean
	public Binding FanoutBinding2() {
		return BindingBuilder.bind(topicQueue2()).to(fanoutExchage());
	}
	/**
	 * Header模式 交换机Exchange
	 * */
	@Bean
	public HeadersExchange headersExchage(){
		return new HeadersExchange(HEADERS_EXCHANGE);
	}
	@Bean
	public Queue headerQueue1() {
		return new Queue(HEADER_QUEUE, true);
	}
	@Bean
	public Binding headerBinding() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("header1", "value1");
		map.put("header2", "value2");
		return BindingBuilder.bind(headerQueue1()).to(headersExchage()).whereAll(map).match();
	}

	
}
