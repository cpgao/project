package com.imooc.mq.rabbitmq;


import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.GetResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 监听消息 Queue消息
 */
@Slf4j
@Service
public class QueueListener {
    @Autowired
    private MQSender mqSender;
//    @Autowired
//    private PlatformTransactionManager transactionManager;
    /**
     * 情况一：测试收消息
     * @param message
     */
    public void queueListener(String message) {
        log.info(" queueListener 收到消息了  message="+message);
    }
    /**
     * 情况二：测试收取同一队列消息，测试结果：query类消息只会收到一次
     * @param message
     */
    public void queueListener1(String message) {
        log.info(" queueListener1 收到消息了  message="+message);
    }
    /**
     * 情况三：消息为error时候，测试错误消息是否会重发，结果：一旦抛错会一直重试
     * （需要配置参数：是否重试？多久重试？重试次数？重试超出是否丢弃？）
     *
     * 情况四：消息为jms时候，测试收到消息的同时，发送消息出去
     * 若中间发送异常，发送消息是否会回滚？
     * @param message
     */
    public void queryMsgListener(String message) {
        log.info(" queryMsgListener 收到消息了  message="+message);
//        if(message.equals("error")){
//            //测试结果，此处抛出异常后会不停重试
//            throw new RuntimeException("我是msg error!");
//        }
//        mqSender.sendJsmMessage(message);
//        //判断是jsm消息，主动抛异常，测试回滚
//        if(message.equals("jms")){
//            throw new RuntimeException("我是jms error!");
//        }

//        //以下为开始jsm事物处理，一旦发送异常，处理回滚，避免
//        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
//        def.setTimeout(15);
//        TransactionStatus status = transactionManager.getTransaction(def);
//        try {
//            mqSender.sendJsmMessage(message);
//            //判断是jsm消息，主动抛异常，测试回滚
//            if(message.equals("jms")){
//                throw new RuntimeException("我是jms error!");
//            }
//            transactionManager.commit(status);
//            log.info(" jms 消息被我提交了 message="+message);
//        } catch (Exception e) {
//            transactionManager.rollback(status);
//            log.info(" jms 消息被我回滚了 message="+message);
//        }
    }
    public void queryJmsListener(String message) {
        log.info(" queryJmsListener 收到消息了  message="+message);
    }
    public void queryNewListener(String message) {
        log.info(" queryNewListener 收到消息了  message="+message);
    }

//    public void retry() throws IOException {
//        //消息消费
//        GetResponse getResponse = null;
//        try {
//            getResponse = rabbitUtil.fetch(DQConstant.CLICK_QUEUE_NAME, false);
//            /**
//             * 业务处理
//             */
//            throw new RuntimeException("错粗了");
//        } catch (Exception e) {
//            if(null != getResponse) {
//                long retryCount = getRetryCount(getResponse.getProps());
//                if(retryCount > 3) {
//                    //重试超过3次的，直接存入失败队列
//                    AMQP.BasicProperties properties = getResponse.getProps();
//                    Map<String, Object> headers = properties.getHeaders();
//                    if(null == headers) {
//                        headers = new HashMap<>();
//                    }
//                    properties.builder().headers(headers);
//                    //rabbitUtil.send(DQConstant.CLICK_FAILED_EXCHANGE_NAME, DQConstant.CLICK_FAILED_ROUTING_KEY, properties, getResponse.getBody());
//                } else {
//                    //重试不超过3次的，加入到重试队列
//                    AMQP.BasicProperties properties = getResponse.getProps();
//                    Map<String, Object> headers = properties.getHeaders();
//                    if(null == headers) {
//                        headers = new HashMap<>();
//                    }
//                    properties.builder().headers(headers);
//                    //rabbitUtil.send(DQConstant.CLICK_RETRY_EXCHANGE_NAME, DQConstant.CLICK_RETRY_ROUTING_KEY, properties, getResponse.getBody());
//                }
//            }
//        }
//        if(null != getResponse) {
//            rabbitUtil.ack(getResponse);
//        }
//    }
//    private long getRetryCount(AMQP.BasicProperties properties) {
//        long retryCount = 0;
//        Map<String, Object> headers = properties.getHeaders();
//        if(null != headers) {
//            if(headers.containsKey("x-death")) {
//                List<Map<String, Object>> deathList = (List<Map<String, Object>>) headers.get("x-death");
//                if(!deathList.isEmpty()) {
//                    Map<String, Object> deathEntry = deathList.get(0);
//                    retryCount = (Long)deathEntry.get("count");
//                }
//            }
//        }
//        return retryCount;
//    }
}
