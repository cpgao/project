package com.imooc.mq.rabbitmq;

import com.imooc.common.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * rmq测试
 */
@Slf4j
@Controller
@RequestMapping("/rmq/")
public class RabbitmqController {

    @Autowired
    private MQSender mqSender;

    @ResponseBody
    @RequestMapping(value = "/queue")
    public ServerResponse<String> hello(){
        mqSender.send("你好，rabbitmq，queue");
        return ServerResponse.createBySuccess("hello，rabbitmq，queue！");
    }

    @ResponseBody
    @RequestMapping(value = "/sendQueue")
    public ServerResponse<String> sendQueue(@RequestParam String message){
        mqSender.sendQueueMessage(message);
        return ServerResponse.createBySuccess("发送成功！message="+message);
    }


}
