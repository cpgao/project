package com.imooc.mq.activemq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


@Service
@Slf4j
public class ActivemqErrorListener implements ErrorHandler {

    @Override
    public void handleError(Throwable t) {
        log.info("异常消息监听 message="+t.getMessage());
    }
}