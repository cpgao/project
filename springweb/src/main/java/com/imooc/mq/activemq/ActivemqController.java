package com.imooc.mq.activemq;

import com.imooc.common.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * amq测试
 */
@Slf4j
@Controller
@RequestMapping("/amq/")
public class ActivemqController {

    @Autowired
    private ActivemqSendMessage activeMqSendMessage;




    @ResponseBody
    @RequestMapping(value = "/queue")
    public ServerResponse<String> hello(){
        activeMqSendMessage.sendActiveQueue(" activemq，hello！");
        return ServerResponse.createBySuccess(" activemq，hello！");
    }
//    http://localhost:8080/amq/sendQueue?message=123
    @ResponseBody
    @RequestMapping(value = "/sendQueue")
    public ServerResponse<String> sendQueue(@RequestParam String message){
        activeMqSendMessage.sendActiveQueue(message);
        return ServerResponse.createBySuccess("发送成功！message="+message);
    }

}
