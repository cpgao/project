package com.imooc.mq.activemq;


import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.util.ByteSequence;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Service
@Slf4j
public class ActivemqListener implements MessageListener {

    @Override
    public void onMessage(Message message){
//        ActiveMQBytesMessage msg = (ActiveMQBytesMessage) message;
//        ByteSequence sequence = msg.getContent();
//        String msgStr = new String(sequence.data);
//        try {
//            msg.acknowledge();
//        } catch (JMSException e) {
//            e.printStackTrace();
//        }
        String text="";
        try {
            TextMessage tm = (TextMessage)(message);
            text=tm.getText();
            //测试异常消息会不会被消费，测试结果：无论接收过程中是否发送异常，消息都已经被消费了
            if(text.indexOf("error")!=-1){
                throw new RuntimeException("我是 接收消息异常： error!");
            }
//            tm.acknowledge();
            log.info("queue1 监听收到一条消息了 message="+text);
        } catch (Exception e) {
            log.error(e.getMessage(),e.toString());
        }
        //测试异常消息会不会被消费，测试结果：无论接收过程中是否发送异常，消息都已经被消费了
        if(text.indexOf("error")!=-1){
            throw new RuntimeException("我是 接收消息异常： error!");
        }
    }
}